'use strict';

angular.module('templates', []);
angular.module('myApp', [
  'ngAnimate',
  'ngRoute',
  'myApp.home'
]).

config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/home'});
}]).

controller('AppCtrl', ['$scope', '$window', '$timeout', function($scope, $window, $timeout){
  var self = this;
}]).


directive('resize', ['$window', '$document', function ($window, $document) {
    return function (scope, element, attrs) {
      var wait = 0;
      attrs.$observe('resize', function(newWait){
        wait = $window.parseInt(newWait || 0);
        angular.element($window).on('resize', $window._.debounce(function() {
          var w = $window,
              x = w.innerWidth,
              y = w.innerHeight;
          scope.$broadcast('resize::resize', {
            innerWidth: x,
            innerHeight: y
          });
        }, wait));
      });
    };
}]);

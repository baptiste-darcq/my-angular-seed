module.exports = function(config){
  config.set({

    basePath : './',

    files : [
      'src/bower_components/angular/angular.js',
      'src/bower_components/angular-route/angular-route.js',
      'src/bower_components/angular-animate/angular-animate.js',
      'src/bower_components/angular-mocks/angular-mocks.js',
      'src/modules/**/*.js'
    ],

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],

    plugins : [
      'karma-chrome-launcher',
      'karma-jasmine'
    ],

  });
};

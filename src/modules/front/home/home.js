angular.module('myApp.home', ['ngAnimate', 'ngRoute']).

config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'templates/home-template.html',
    controller: 'HomeController'
  });
}]).

controller('HomeController', ['$scope', function($scope) {
  var self = this;
  self.name = 'home';
}]).

animation('.anime', [function(){
  return {
    addClass: function(element, className, done){
      // TweenMax.to(element, .8, {y: 0, ease:Expo.easeOut, onComplete:done});
    },
    removeClass: function(element, className, done){
      // TweenMax.to(element, .8, {y: $(element).height() - 5, ease:Expo.easeOut, onComplete:done});
    }
  }
}]);

'use strict';

/* Load all necessary plugins */
var gulp          = require('gulp'),
    glob          = require('glob'),
    gutil         = require('gulp-util'),
    plumber       = require('gulp-plumber'),
    rename        = require('gulp-rename'),
    filter        = require('gulp-filter'),
    sourcemaps    = require('gulp-sourcemaps'),
    browserSync   = require('browser-sync'),
    cache         = require('gulp-cache'),
    concat        = require('gulp-concat'),
    del           = require('del'),
    /* Plugins working on Html files */
    htmlreplace   = require('gulp-html-replace'),
    minifyhtml    = require('gulp-minify-html'),
    /* Plugins working on Javascript files */
    uglify        = require('gulp-uglify'),
    /* Plugins working on Images */
    imagemin      = require('gulp-imagemin'),
    /* Plugins working on Stylesheets */
    csso          = require('gulp-csso'),
    autoprefixer  = require('gulp-autoprefixer'),
    sass          = require('gulp-sass'),
    /* Plugins working on gulp CLI arguments */
    options       = require('minimist')(process.argv.slice(2));

/* Defines Paths and Glob */
var paths = {
  index: 'src/index.html',
  styles: {
    compile: [ 'src/styles/style.scss', 'src/modules/**/*.scss' ],
    watch: [ 'src/styles/*.scss', 'src/modules/**/*.scss' ]
  },
  vendor: [
    'src/bower_components/lodash/dist/lodash.js',
    'src/bower_components/gsap/src/minified/TweenLite.js'
  ],
  scripts: [ 'src/scripts/app.js', 'src/modules/**/*.js', '!src/modules/**/*-test.js' ],
  templates: 'src/modules/**/*.html',
  assets: 'src/assets/**/*',
  dev: 'dist/dev',
  prod: 'dist/prod'
};

/* Run browser-sync */
gulp.task('browser-sync', function() {
  var http = require('http');
  http.get({ host: 'localhost', port: 3000 }, function(response){
    // WITH AN EXPRESS SERVER RUNNING ON PORT 3000
    browserSync.init(null, {
      proxy: 'http://localhost:3000',
      port: 4000
    });
  }).on("error", function(error){
    // WITHOUT AN EXPRESS SERVER RUNNING
    browserSync({
      server: {
        baseDir: options.production ? paths.prod : paths.dev
      }
    });
  });
});

/* Find all ".html" file in src/modules and copy them in a "/templates" folder */
gulp.task('templates', function () {
  return gulp.src(paths.templates)
    .pipe(options.production ? minifyhtml() : gutil.noop())
    .pipe(rename(function(path){ path.dirname = ''; return path; }))
    .pipe(gulp.dest((options.production ? paths.prod : paths.dev) + '/templates'));
});

/* Concatenate files into one "style.css" file after do many task width rework */
gulp.task('styles', function() {
  return gulp.src(paths.styles.compile)
    .pipe(options.production ? gutil.noop() : plumber())
    .pipe(options.production ? gutil.noop() : sourcemaps.init())
    .pipe(sass({ errLogToConsole: true }))
    .pipe(concat('style.css'))
    .pipe(autoprefixer({ browsers: ['last 2 version', 'ie 9', 'ios 6', 'android 4'] } ))
    .pipe(options.production ? gutil.noop() : sourcemaps.write('./'))
    .pipe(options.production ? rename({ suffix: '.min' }) : gutil.noop())
    .pipe(options.production ? csso() : gutil.noop())
    .pipe(options.production ? gulp.dest(paths.prod + '/styles') : gulp.dest(paths.dev + '/styles'))
    .pipe(browserSync.reload({stream:true}));
});

/* Concatenate and optimize js library in one "vendors.js" file */
gulp.task('vendor', function() {
  return gulp.src(paths.vendor)
    .pipe(options.production ? gutil.noop() : sourcemaps.init(''))
    .pipe(concat('vendor.js'))
    .pipe(options.production ? rename({ suffix: '.min' }) : gutil.noop())
    .pipe(options.production ? uglify() : gutil.noop())
    .pipe(options.production ? gutil.noop() : sourcemaps.write('./'))
    .pipe(options.production ? gulp.dest(paths.prod + '/scripts') : gulp.dest(paths.dev + '/scripts'));
});

/* Concatenate and optimize js application in one "app.js" file */
gulp.task('scripts', function() {
  return gulp.src(paths.scripts)
    .pipe(options.production ? gutil.noop() : plumber())
    .pipe(options.production ? gutil.noop() : sourcemaps.init())
    .pipe(concat('app.js'))
    .pipe(options.production ? gutil.noop() : sourcemaps.write('./'))
    .pipe(options.production ? rename({ suffix: '.min' }) : gutil.noop())
    .pipe(options.production ? uglify() : gutil.noop())
    .pipe(options.production ? gulp.dest(paths.prod + '/scripts') : gulp.dest(paths.dev + '/scripts'));
});

/* Modify some js and css request in our index.html and copy it to our dist folder */
gulp.task('index', function() {
  return gulp.src('src/index.html')
    .pipe(options.production ? htmlreplace({
        'stylesheets': './styles/style.min.css',
        'javascripts': ['./scripts/vendor.min.js', './scripts/app.min.js']
    }) : htmlreplace({
        'stylesheets': './styles/style.css',
        'javascripts': ['./scripts/vendor.js', './scripts/app.js']
    }))
    .pipe(gulp.dest(options.production ? paths.prod : paths.dev));
});

/* Copy all folder/file in src/assets to our dist folder and minify images */
gulp.task('assets', function() {
  var imgFilter = filter('images/*');
  return gulp.src(paths.assets)
    .pipe(imgFilter)
    .pipe(options.production ? cache(imagemin({ optimizationLevel: 7, progressive: true, interlaced: true })) : gutil.noop())
    .pipe(imgFilter.restore())
    .pipe(gulp.dest(options.production ? paths.prod : paths.dev));
});

/* Clean the dist folder */
gulp.task('clean', function(cb) {
  del(options.production ? paths.prod : paths.dev, cb);
});

/* Default task */
gulp.task('default', ['clean'], function() {
  gulp.start('assets', 'templates', 'styles', 'vendor', 'scripts', 'index', 'browser-sync', 'watch');
});

/* Watch all files except js library */
gulp.task('watch', function() {
  gulp.watch(paths.styles.watch, ['styles']);
  gulp.watch(paths.templates, ['templates', browserSync.reload]);
  gulp.watch(paths.scripts, ['scripts', browserSync.reload]);
  gulp.watch(paths.assets, ['assets', browserSync.reload]);
  gulp.watch(paths.index, ['index', browserSync.reload]);
});

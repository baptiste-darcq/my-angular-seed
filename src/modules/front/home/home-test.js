'use strict';

describe('myApp.home module', function() {

  var scope,
  controller;

  beforeEach(module('myApp.home'));

  describe('home controller', function(){
    beforeEach(inject(function($controller) {
      controller = $controller('HomeController', {$scope: scope});
    }));
    it('sets the name', function () {
      expect(controller.name).toBe('home');
    });
    it('should have been defined', function() {
      expect(controller).toBeDefined();
    });

  });
});
